import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AuthorPage } from '../pages/author/author';
import { CurriculumPage } from '../pages/curriculum/curriculum';
import { LibraryPage } from '../pages/library/library';
import { BooksProvider } from '../providers/books/books';
import { DetallePage } from '../pages/detalle/detalle';

@NgModule({
  declarations: [
    MyApp,
    HomePage, AuthorPage, CurriculumPage, LibraryPage, DetallePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage, AuthorPage, CurriculumPage, LibraryPage, DetallePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BooksProvider
  ]
})
export class AppModule {}
