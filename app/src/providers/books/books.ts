import { Injectable } from '@angular/core';

/*
  Generated class for the BooksProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BooksProvider {

  private books:{title:string, year:string, author:string,
            isbn:string, editorial:string, cover:string}[];

  constructor() {
    this.books = [{title:"La colmena", year:"1951", author:"Camilo José Cela Trulock", isbn:"843992688X", editorial:"Anaya", cover:"lacolmena.jpg"},
    {title:"La galatea", year:"1585", author:"Miguel de Cervantes Saavedra", isbn:"0936388110", editorial:"Anaya", cover:"lagalatea.jpg"},
    {title:"El ingenioso hidalgo don Quijote de la Mancha", year:"1605", author:"Miguel de Cervantes Saavedra", isbn:"0844273619", editorial:"Anaya", cover:"donquijote.jpg"},
    {title:"La dorotea", year:"1632", author:"Félix Lope de Vega y Carpio", isbn:"847039360X", editorial:"Anaya", cover:"ladorotea.jpg"},
    {title:"La dragontea", year:"1602", author:"Félix Lope de Vega y Carpio", isbn:"8437624045", editorial:"Anaya", cover:"ladragontea.jpg"}]
    console.log('Hello BooksProvider Provider');
  }
  getLibros(){
    return this.books;
  }

}
