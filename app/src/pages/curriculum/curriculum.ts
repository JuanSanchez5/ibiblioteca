import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CurriculumPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-curriculum',
  templateUrl: 'curriculum.html',
})
export class CurriculumPage {
  listaTrabajos:{fecha:string, desc:string}[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.listaTrabajos = [{fecha: "2017 - actualidad", desc:"Desarrollador full-stack en Samart & fun"},
    {fecha: "2017 - actualidad", desc:"Master en Desarrollo de Software para Dispositivos Móviles"},
    {fecha: "2012 - 2017", desc:"Grado en Ingeniería Multimedia"}];
    console.log(this.listaTrabajos);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CurriculumPage');
  }

}
