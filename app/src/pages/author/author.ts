import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CurriculumPage } from '../curriculum/curriculum';
/**
 * Generated class for the AuthorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-author',
  templateUrl: 'author.html',
})
export class AuthorPage {

  autor:{
    name:string,
    email:string,
    twitter:string,
    phone:string
};

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.autor = {name: "Juan Sánchez Marín", email: "jsm114@alu.ua.es", twitter: "@twitter", phone: "123456789"};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AuthorPage');
  }
  goCurriculum():void {
    this.navCtrl.push(CurriculumPage);
  }

}
