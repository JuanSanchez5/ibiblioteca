import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BooksProvider } from '../../providers/books/books';
import { DetallePage } from '../detalle/detalle';

/**
 * Generated class for the LibraryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-library',
  templateUrl: 'library.html',
})
export class LibraryPage {
  listaLibros:any[]

  constructor(public navCtrl: NavController, public navParams: NavParams, public books: BooksProvider) {
    this.listaLibros = books.getLibros();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LibraryPage');
  }
  goDetalle(libro){
    this.navCtrl.push(DetallePage, {libro: libro});
  }

}
