import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LibraryPage } from '../library/library';
import { AuthorPage } from '../author/author';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }
  goIBiblioteca(): void {
    this.navCtrl.push(LibraryPage);
  }
  goAuthor(): void {
    this.navCtrl.push(AuthorPage);
  }

}
